import { Logger } from '@nestjs/common';

import { OnGatewayConnection, OnGatewayDisconnect, SubscribeMessage, WebSocketGateway, WebSocketServer } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';

@WebSocketGateway({ cors: { origin: '*' } })
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer()
  server: Server

  private logger: Logger = new Logger('AppGateway')

  @SubscribeMessage('messageServer')
  handleMessage(client: Socket, payload: any): void {
    this.server.emit('messageClient', payload)
    console.log(payload)
    return payload
  }

  init(){
    this.logger.log('init ws')
  }

  handleDisconnect(client: Socket){
    this.logger.log(`Client is disconnection: ${client.id}`)
  }

  handleConnection(client: Socket, ...args: any[]){
    this.logger.log(`Client is connection: ${client.id}`)
  }
}
