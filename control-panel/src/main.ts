import { createApp } from 'vue'
import App from './App.vue'

import ElementUI from 'element-plus'
import 'element-plus/dist/index.css'

import router from './router'
import store from './store'

import '@/assets/styles/reset.style.scss'

const app = createApp(App)

app.use(store)
app.use(router)
app.use(ElementUI)

app.mount('#app')
